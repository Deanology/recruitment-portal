﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RecruitmentPortal.Services.Entities
{
    public class Customer
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Email is required")]
        [EmailAddress]
        public string Email { get; set; }
        public string Password { get; set; }
        [Required(ErrorMessage = "First Name is required")]
        [Display(Name="First Name")]
        [StringLength(160)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required")]
        [Display(Name="Last Name")]
        [StringLength(160)]
        public string Lastnamme { get; set; }
        [Display(Name = "DOB")]
        [Column(TypeName = "datetime2")]
        public DateTime? DateOfBirth { get; set; }
        [Required(ErrorMessage = "Gender is required")]
        [StringLength(24)]
        [Display(Name = "Gender")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "State is required")]
        [StringLength(200, MinimumLength =2)]
        public string State { get; set; }
        [Required(ErrorMessage = "Address is required")]
        [StringLength(200, MinimumLength =10)]
        public string Address { get; set; }
        [Required(ErrorMessage = "Phone is required")]
        [StringLength(24)]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [DataType(DataType.Upload)]
        [Display(Name = "Upload Image")]
        [Required(ErrorMessage ="Please choose file to upload. ")]
        public string file { get; set; }
        //still cant remember why i added this
        /*public virtual ICollection<Applications> Applications { get; set; }*/
    }
}