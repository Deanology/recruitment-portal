﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RecruitmentPortal.Services.Entities
{
    public class Category
    {
        public int Id { get; set; }
        [Required, StringLength(100), Display(Name = "Category Name")]
        public string CategoryName { get; set; }
        public virtual ICollection<Jobs> Jobs { get; set; }
    }
}