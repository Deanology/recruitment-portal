﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RecruitmentPortal.Services.Entities
{
    public class Jobs
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Job Title is required")]
        [Display(Name = "Job Title")]
        [StringLength(160)]
        public string JobTitle { get; set; }
        [Required(ErrorMessage = "Job Type is required")]
        [Display(Name = "Job Type")]
        [StringLength(160)]
        public string JobType { get; set; }
        [Required(ErrorMessage = "Job Position is required")]
        [Display(Name = "Job Position")]
        [StringLength(160)]
        public string JobPosition { get; set; }
        [Required(ErrorMessage = "Employer Name is required")]
        [Display(Name = "Employer Name")]
        [StringLength(160)]
        public string EmployerName { get; set; }
        [Required(ErrorMessage = "Employer Location is required")]
        [Display(Name = "Employer Location")]
        [StringLength(160)]
        public string EmployerLocation { get; set; }

        public string JobDescription { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? UploadDate { get; set; }
        public int? CategoryID { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Applications> Applications { get; set; }
    }
}