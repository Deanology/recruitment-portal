﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RecruitmentPortal.Services.Entities
{
    public enum ApplicationStatus
    {
        Pending,
        Rejected,
        Accepted
    }
    public class Applications
    {
        public int Id { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? DateOfApplication { get; set; }
        public string file { get; set; }
        public ApplicationStatus ApplicationStatus { get; set; }
        public int JobsId { get; set; }
        public virtual Jobs Jobs { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

    }
}