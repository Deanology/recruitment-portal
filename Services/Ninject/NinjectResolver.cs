﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Ninject;
using Ninject.Web.Common;
using RecruitmentPortal.Models;
using RecruitmentPortal.Services.Interfaces;
using RecruitmentPortal.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http.Dependencies;
using System.Web.Mvc;

namespace RecruitmentPortal.Services.Ninject
{
    public class NinjectResolver : System.Web.Http.Dependencies.IDependencyResolver, System.Web.Mvc.IDependencyResolver
    {
        private IKernel _ninjectKernel;
        public NinjectResolver(IKernel ninjectKernel)
        {
            _ninjectKernel = ninjectKernel;
            AddBindings();
        }
        private void AddBindings()
        {
            _ninjectKernel.Bind<IMail>().To<EmailSenderRepository>();
            _ninjectKernel.Bind<IAdmin>().To<AdminRepository>();
            _ninjectKernel.Bind<IDocuments>().To<DocumentRepository>();
            _ninjectKernel.Bind<IApplication>().To<ApplicationRepository>();
            _ninjectKernel.Bind<IJobs>().To<JobsRepository>();
            _ninjectKernel.Bind<ICustomer>().To<CustomerRepository>();
            _ninjectKernel.Bind<ICategory>().To<CategoryRepository>();
            _ninjectKernel.Bind<IRoleRepository>().To<RoleRepository>();
            _ninjectKernel.Bind<ApplicationDbContext>().ToSelf();

            _ninjectKernel.Bind<ApplicationUserManager>().ToMethod(c => HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>()).InRequestScope();
            _ninjectKernel.Bind<ApplicationSignInManager>().ToSelf();

            _ninjectKernel.Bind<IRoleStore<IdentityRole, string>>().To<RoleStore<IdentityRole, string, IdentityUserRole>>();
            _ninjectKernel.Bind<RoleManager<IdentityRole, string>>().ToSelf();
            _ninjectKernel.Bind<IUserStore<ApplicationUser>>().To<UserStore<ApplicationUser>>();
            _ninjectKernel.Bind<IAuthenticationManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Authentication).InRequestScope();

        }
        public IDependencyScope BeginScope()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public object GetService(Type serviceType)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<object> GetServices(Type serviceType) => _ninjectKernel.GetAll(serviceType);
    }
}