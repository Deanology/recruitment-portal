﻿using MailKit.Net.Smtp;
using MimeKit;
using RecruitmentPortal.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RecruitmentPortal.Services.Repositories
{
    public class EmailSenderRepository : IMail
    {
        public string Id { get; set; }
        public string GetCurrentUser()
        {
            Id = HttpContext.Current.User.Identity.Name;
            return Id;
        }

        public string Heading()
        {
            string heading = "Automatic reply:";
            return heading;
        }

        public string Heading2()
        {
            string heading = "Request Accepted:";
            return heading;
        }

        public string Heading3()
        {
            string heading = "Request Rejected:";
            return heading;
        }

        public string Message()
        {
            string message = "Dear Candidate, Thank you for your interest " +
                                 "This is to confirm receipt of your application. Our recruitment team will review " +
                                 "your application and contact you on next steps if we find your profile suitable for any of our vacant role." +
                                 "Best regards,";
            return message;
        }

        public string Message2()
        {
            string message = "Dear Candidate, Thank you for your interest " +
                                 "This is to confirm receipt of your application. Our recruitment team has reviewed " +
                                 "your application and it has been accepted. You will be contacted on the next steps in completing the application." +
                                 "Congratulations";
            return message;
        }

        public string Message3()
        {
            string message = "Dear Candidate, Thank you for your interest " +
                                 "This is to confirm receipt of your application. Our recruitment team has reviewed " +
                                 "your application was not considered. We will not be able to carry on with your application" +
                                 "Better luck next time";
            return message;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            try
            {
                // Plug in your email service here to send an email.
                var emailMessage = new MimeMessage();

                //
                emailMessage.From.Add(new MailboxAddress("TEST SMTP", "okechukwuidam516@gmail.com"));
                emailMessage.To.Add(new MailboxAddress("TEST SMTP", email));
                emailMessage.Subject = subject;
                emailMessage.Body = new TextPart("plain") { Text = message };

                await Task.Run(() =>
                {
                  
                    using (var client = new SmtpClient())
                    {
                        client.Connect("smtp.gmail.com", 587, false);

                        // Note: since we don't have an OAuth2 token, disable
                        // the XOAUTH2 authentication mechanism.
                        client.AuthenticationMechanisms.Remove("XOAUTH2");

                        // Note: only needed if the SMTP server requires authentication
                        client.Authenticate("okechukwuidam516@gmail.com", "QWerty55!");

                        client.Send(emailMessage);
                        client.Disconnect(true);

                    }
                });
            }
            catch(Exception e)
            {
                throw e;
            }
            
        }
    }
}