﻿using RecruitmentPortal.Models;
using RecruitmentPortal.Services.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RecruitmentPortal.Services.Repositories
{
    public class AdminRepository : Interfaces.IAdmin
    {
        ApplicationDbContext _db;
        public AdminRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public int AcceptedRequests()
        {
            //get the number of accepted applications in the database
            int? count = (from applications in _db.Applications
                          where applications.ApplicationStatus == ApplicationStatus.Accepted
                          select applications).Count();

            //Return 0 if all entries are null
            return count ?? 0;
        }

        public int Applicants()
        {
            //get the number of applicants in the database
            int? count = (from applicants in _db.Applications
                          select (int?)applicants.Id).Count();
            //Return 0 if all entries are null
            return count ?? 0;
        }

        public int Categories()
        {
            //get the number of jobs categories in the database
            int? count = (from categories in _db.Categories
                          select (int?)categories.Id).Count();
            //Return 0 if all entries are null
            return count ?? 0;
        }

        public IEnumerable<Applications> GetApplications(int id)
        {
            return _db.Applications.ToList().Where(c => c.JobsId == id);
        }

        public IEnumerable<Customer> GetCustomers()
        {
            return _db.Customers.ToList();
        }

        public IEnumerable<Jobs> GetJobs()
        {
            return _db.Jobs.ToList();
        }

        public int NumberOfJobs()
        {
            //get the number of jobs in the database
            int? count = (from jobs in _db.Jobs
                          select (int?)jobs.Id).Count();
            //Return 0 if all entries are null
            return count ?? 0;
        }

        public int PendingRequests()
        {
            //get the number of pending applications in the database
            int? count = (from applications in _db.Applications
                          where applications.ApplicationStatus == ApplicationStatus.Pending
                          select applications).Count();
                          
            //Return 0 if all entries are null
            return count ?? 0;
        }

        public int RejectedRequests()
        {
            //get the number of rejected applications in the database
            int? count = (from applications in _db.Applications
                          where applications.ApplicationStatus == ApplicationStatus.Rejected
                          select applications).Count();

            //Return 0 if all entries are null
            return count ?? 0;
        }

        public int Users()
        {
            //get the number of users in the database
            int? count = (from customers in _db.Customers
                          select (int?)customers.Id).Count();
            //Return 0 if all entries are null
            return count ?? 0;
        }
    }
}