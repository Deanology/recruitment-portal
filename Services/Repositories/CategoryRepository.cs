﻿using RecruitmentPortal.Models;
using RecruitmentPortal.Services.Entities;
using RecruitmentPortal.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecruitmentPortal.Services.Repositories
{
    public class CategoryRepository : ICategory
    {
        ApplicationDbContext _db;
        //dependency injection made possible by ninject
        public CategoryRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteCategoryById(int categoryId)
        {
            Category category = GetCategoryById(categoryId);
            _db.Categories.Remove(category);
            _db.SaveChanges();
        }

        public IEnumerable<Category> GetCategories()
        {
            //return all categories in the database
            return _db.Categories.ToList();
        }

        public Category GetCategoryById(int? categoryId)
        {
            return _db.Categories.Find(categoryId.Value);
        }

        public void InsertCategory(Category category)
        {
            _db.Categories.Add(category);
            _db.SaveChanges();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void UpdateCategory(Category category)
        {
            _db.Entry(category).State = System.Data.Entity.EntityState.Modified;
            _db.SaveChanges();
        }
    }
}