﻿using Microsoft.AspNet.Identity;
using RecruitmentPortal.Models;
using RecruitmentPortal.Services.Entities;
using RecruitmentPortal.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace RecruitmentPortal.Services.Repositories
{
    public class CustomerRepository : ICustomer
    {
        public string Id { get; set; }
        private IRoleRepository _roleRepository;
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;
        private ApplicationDbContext _db;
        public CustomerRepository(ApplicationDbContext db, IRoleRepository roleRepository, ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            _db = db;
            _roleRepository = roleRepository;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public bool ApplicaantRegister(Customer customer, HttpPostedFileBase file)
        {
            //check if the customer entity is not null
            if(customer != null && file != null)
            {
                //if not null, add user first to the AppNetUser Table
                var user = new ApplicationUser { Email = customer.Email, UserName = customer.Email };
                var result = _userManager.CreateAsync(user, customer.Password).Result;
                //if successful
                if (result.Succeeded)
                {
                    string path = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/ImageUploaded/"), Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                     
                    var customers = new Customer()
                    {
                        Email = user.Email,
                        FirstName = customer.FirstName,
                        Lastnamme = customer.Lastnamme,
                        Address = customer.Address,
                        DateOfBirth = customer.DateOfBirth,
                        Gender = customer.Gender,
                        State = customer.State,
                        PhoneNumber = customer.PhoneNumber,
                        file = file.FileName
                    };
                    
                    var role = _roleRepository.CreateRole("APPLICANT");
                    if (role)
                    {
                        _userManager.AddToRole(user.Id, "APPLICANT");
                    }
                    _db.Customers.Add(customers);
                    _db.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public bool ApplicantLogin(LoginViewModel model)
        {
            if (model != null)
                return true;
            return false;
        }

        public bool EmployerRegister(Customer customer)
        {
            throw new NotImplementedException();
        }

        public Customer GetCustomerById(int? customerId)
        {
            return _db.Customers.Find(customerId.Value);
        }

        public void UpdateProfile(Customer customer, HttpPostedFileBase file)
        {
            string path = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/ImageUploaded/"), Path.GetFileName(file.FileName));
            file.SaveAs(path);

            var customers = new Customer()
            {
                Email = customer.Email,
                FirstName = customer.FirstName,
                Lastnamme = customer.Lastnamme,
                Address = customer.Address,
                DateOfBirth = customer.DateOfBirth,
                Gender = customer.Gender,
                State = customer.State,
                PhoneNumber = customer.PhoneNumber,
                file = file.FileName
            };
            _db.Entry(customer).State = System.Data.Entity.EntityState.Modified;
            _db.SaveChanges();
        }
        public int CustomerId()
        {
            Id = HttpContext.Current.User.Identity.Name;
            //get the id of the current logged in user
            int? count = (from customer in _db.Customers
                          where customer.Email == Id
                          select (int?)customer.Id).SingleOrDefault();
            //Return 0 if all entries are null
            return count ?? 0;
        }
    }
}