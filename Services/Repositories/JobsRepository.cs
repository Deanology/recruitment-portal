﻿using RecruitmentPortal.Models;
using RecruitmentPortal.Services.Entities;
using RecruitmentPortal.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace RecruitmentPortal.Services.Repositories
{
    public class JobsRepository : IJobs
    {
        ApplicationDbContext _db;
        public JobsRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteJobsById(int jobId)
        {
            Jobs jobs = GetJobsById(jobId);
            _db.Jobs.Remove(jobs);
            _db.SaveChanges();
        }

        public Jobs GetJobsById(int? jobId)
        {
            return _db.Jobs.Find(jobId.Value);
        }

        public IEnumerable<Jobs> GetJobs()
        {
            //return all jobs in the database
            return _db.Jobs.Include(p => p.Category);
        }

        public void InsertJobs(Jobs jobs)
        {
            _db.Jobs.Add(jobs);
            _db.SaveChanges();
        }

        public void UpdateJobs(Jobs jobs)
        {
            _db.Entry(jobs).State = EntityState.Modified;
            _db.SaveChanges();
        }
        public DbSet<Category> Any()
        {
            return _db.Categories;
        }

        public IEnumerable<Jobs> LimitedJobs()
        {
            return _db.Jobs.OrderByDescending(x => x.Id).Take(6).ToList();
            /* return _db.Jobs.Include(p => p.Category);*/
        }

        /*public IQueryable<Jobs> SearchResult(string keyword, string location, string category) { 
            var result = _db.Jobs.AsQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                result = result.Where(s => s.JobTitle.Contains(keyword));
            }
            if (!string.IsNullOrEmpty(location))
            {
                result = result.Where(s => s.EmployerLocation.Contains(location));
            }
            if (!string.IsNullOrEmpty(category))
            {
                result = result.Where(s => s.Category.CategoryName.Contains(category));
            }
            return result;
        }*/
    }
}