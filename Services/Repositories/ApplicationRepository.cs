﻿using RecruitmentPortal.Models;
using RecruitmentPortal.Services.Entities;
using RecruitmentPortal.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecruitmentPortal.Services.Repositories
{
    public class ApplicationRepository : IApplication
    {
        public string Id { get; set; }
        ApplicationDbContext _db;
        //dependency injection made possible by ninject
        public ApplicationRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Applications> GetApplication()
        {
            //return all details in the Application database
            return _db.Applications.ToList();
        }

        public Applications GetApplicationById(int? ApplicationId)
        {
            return _db.Applications.Find(ApplicationId.Value);
        }

        public List<Customer> GetCustomers()
        {         
            throw new NotImplementedException();
        }

        //get all the detail of a particular job advertisement
        public List<Jobs> GetJobs(int id)
        {
            return _db.Jobs.Where(c => c.Id == id).ToList();
        }


        public IEnumerable<Applications> SpecificApplication()
        {
            Id = HttpContext.Current.User.Identity.Name;
            return _db.Applications.ToList().Where(c => c.Customer.Email == Id);
        }

        public bool InsertApplications(Applications application, HttpPostedFileBase file, int id)
        {
            if (application != null && file != null)
            {
                Id = HttpContext.Current.User.Identity.Name;
                string path = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Documents/"), Path.GetFileName(file.FileName));
                file.SaveAs(path);
                _db.Applications.Add(new Applications
                {
                    DateOfApplication = DateTime.Now.Date,
                    ApplicationStatus = ApplicationStatus.Pending,
                    Jobs = _db.Jobs.SingleOrDefault(p => p.Id == id),
                    Customer = _db.Customers.SingleOrDefault(p => p.Email == Id),
                    file = file.FileName
                });
                _db.SaveChanges();

                return true;
            }
            return false;
        }

        public string AcceptApplications(int id)
        {
            /*Id = HttpContext.Current.User.Identity.Name;*/
            try
            {
                var application = _db.Applications.FirstOrDefault(c => c.Id == id);
                if (application != null)
                {
                    if(application.ApplicationStatus != ApplicationStatus.Accepted)
                    {
                        application.ApplicationStatus = ApplicationStatus.Accepted;
                        _db.Entry(application).State = EntityState.Modified;
                        _db.SaveChanges();
                        
                    }
                    else
                    {
                        //do something
                    }
                    return application.Customer.Email;
                    
                }
                else
                {
                    throw new Exception(" not found");
                }
            }
            catch (Exception exp)
            {
                throw new Exception("ERROR: Unable to Accept Application - " + exp.Message.ToString(), exp);
            }
        }

        public string RejectApplication(int id)
        {
            try
            {
                var application = _db.Applications.FirstOrDefault(c =>c.Id == id);
                if (application != null)
                {
                    if (application.ApplicationStatus != ApplicationStatus.Rejected)
                    {
                        application.ApplicationStatus = ApplicationStatus.Rejected;
                        _db.Entry(application).State = EntityState.Modified;
                        _db.SaveChanges();
                    }
                    else
                    {
                        //do something
                    }
                    return application.Customer.Email;
                }
                else
                {
                    throw new Exception(" not found");
                }
            }
            catch (Exception exp)
            {
                throw new Exception("ERROR: Unable to Reject Application - " + exp.Message.ToString(), exp);
            }
        }

        public FileContentResult GetPDF(int id)
        {
            var pdf = _db.Applications.Where(c => c.Id == id).SingleOrDefault();
            var myfile = System.IO.File.ReadAllBytes(pdf.file);
            return new FileContentResult(myfile, "application/pdf");
            /*var result = new FileContentResult(pdf.file.ToArray(), pdf.file.ContentType);*/
            /*result.FileDownloadName = pdf.file;*/
            /*return result;*/
        }
    }
}