﻿using Microsoft.Owin;
using RecruitmentPortal.Migrations;
using RecruitmentPortal.Models;
using RecruitmentPortal.Services.Entities;
using RecruitmentPortal.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace RecruitmentPortal.Services.Repositories
{
    public class DocumentRepository : IDocuments
    {
        ApplicationDbContext _db;
        //dependency injection made possible by ninject
        public DocumentRepository(ApplicationDbContext db)
        {
            _db = db;
        }


        public bool InsertDoc(Document document, HttpPostedFileBase file)
        {
            if(document != null && file != null)
            {
                string path = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/ImageUploaded/"), Path.GetFileName(file.FileName));
                file.SaveAs(path);
                _db.Documents.Add(new Document
                {
                    Title = document.Title,
                    Description = document.Description,
                    file = file.FileName
                });
                _db.SaveChanges();
                return true;
            }
            return false;
        }
    }
}