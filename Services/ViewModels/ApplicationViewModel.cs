﻿using RecruitmentPortal.Services.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecruitmentPortal.Services.ViewModels
{
    public class ApplicationViewModel
    {
        public List<Customer> Customers { get; set; }
        public List<Jobs> Jobs { get; set; }
        public string file { get; set; }
        public int CustomerId { get; set; }
    }
}