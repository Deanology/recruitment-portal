﻿using RecruitmentPortal.Services.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecruitmentPortal.Services.ViewModels
{
    public class ApplicantViewModel
    {
        public List<Customer> Customers { get; set; }
        public List<Jobs> Jobs { get; set; }
        public int NumberOfCustomers { get; set; }
        public int NumberOfJobs { get; set; }
        public int NumberOfCategories { get; set; }
        public int AcceptedRequests { get; set; }
        public int RejectedRequests { get; set; }
        public int PendingRequests { get; set; }
        public int Categories { get; set; }
        public int Applicants { get; set; }
        public int Users { get; set; }
    }
}