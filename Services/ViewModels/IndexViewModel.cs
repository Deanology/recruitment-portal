﻿using RecruitmentPortal.Services.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecruitmentPortal.Services.ViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<Jobs> Jobs { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}