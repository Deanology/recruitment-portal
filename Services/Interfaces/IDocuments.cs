﻿
using RecruitmentPortal.Services.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace RecruitmentPortal.Services.Interfaces
{
    public interface IDocuments
    {
        bool InsertDoc(Document documents, HttpPostedFileBase file);
    }
}
