﻿using RecruitmentPortal.Services.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecruitmentPortal.Services.Interfaces
{
    public interface ICategory
    {
        IEnumerable<Category> GetCategories();
        Category GetCategoryById(int? categoryId);
        void InsertCategory(Category category);
        void DeleteCategoryById(int categoryId);
        void UpdateCategory(Category category);
        void Save();
    }
}
