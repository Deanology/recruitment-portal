﻿using RecruitmentPortal.Models;
using RecruitmentPortal.Services.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace RecruitmentPortal.Services.Interfaces
{
    public interface ICustomer
    {
        bool ApplicaantRegister(Customer customer, HttpPostedFileBase file);
        bool EmployerRegister(Customer customer);
        bool ApplicantLogin(LoginViewModel model);
        void UpdateProfile(Customer customer, HttpPostedFileBase file);
        Customer GetCustomerById(int? customerId);
        int CustomerId();
    }
}
