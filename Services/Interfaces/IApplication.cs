﻿using RecruitmentPortal.Migrations;
using RecruitmentPortal.Services.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace RecruitmentPortal.Services.Interfaces
{
    public interface IApplication
    {
        List<Jobs> GetJobs(int id);
        List<Customer> GetCustomers();
        IEnumerable<Applications> GetApplication();
        IEnumerable<Applications> SpecificApplication();
        Applications GetApplicationById(int? ApplicationId);
        bool InsertApplications(Applications applications, HttpPostedFileBase file, int id);
        string AcceptApplications(int id);
        string RejectApplication(int id);
        FileContentResult GetPDF(int id);
    }
}
