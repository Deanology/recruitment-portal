﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecruitmentPortal.Services.Interfaces
{
    public interface IMail
    {
        Task SendEmailAsync(string email, string subject, string message);
        string GetCurrentUser();
        string Heading();
        string Message();
        string Heading2();
        string Message2();
        string Heading3();
        string Message3();
    }
}
