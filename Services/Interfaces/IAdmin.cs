﻿using RecruitmentPortal.Services.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecruitmentPortal.Services.Interfaces
{
    public interface IAdmin
    {
        IEnumerable<Jobs> GetJobs();
        IEnumerable<Applications> GetApplications(int id);
        IEnumerable<Customer> GetCustomers();
        int NumberOfJobs();
        int AcceptedRequests();
        int RejectedRequests();
        int PendingRequests();
        int Categories();
        int Applicants();
        int Users();
    }
}
