﻿using RecruitmentPortal.Services.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace RecruitmentPortal.Services.Interfaces
{
    public interface IJobs
    {
        IEnumerable<Jobs> GetJobs();
        IEnumerable<Jobs> LimitedJobs();
        Jobs GetJobsById(int? jobId);
        void InsertJobs(Jobs job);
        void DeleteJobsById(int jobtId);
        void UpdateJobs(Jobs job);
        DbSet<Category> Any();
        
    }
}
