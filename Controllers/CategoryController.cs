﻿using RecruitmentPortal.Services.Entities;
using RecruitmentPortal.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecruitmentPortal.Controllers
{
    [Authorize(Roles ="Admin")]
    public class CategoryController : Controller
    {
        ICategory _iCategory;
        public CategoryController(ICategory iCategory)
        {
            _iCategory = iCategory;
        }
        // GET: Category
        public ActionResult Index()
        {
           var category =  _iCategory.GetCategories();
            return View(category);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Category category)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Error = "Invalid Request sent, Try Again";
                return View();
            }
            _iCategory.InsertCategory(category);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Details(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var category = _iCategory.GetCategoryById(id.Value);
                return View(category);
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var category = _iCategory.GetCategoryById(id.Value);
                return View(category);
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        [HttpPost]
        public ActionResult Edit(Category category)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                _iCategory.UpdateCategory(category);
                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var category = _iCategory.GetCategoryById(id.Value);
                return View(category);
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _iCategory.DeleteCategoryById(id);
            return RedirectToAction("Index");
        }
    }
}