﻿using RecruitmentPortal.Models;
using RecruitmentPortal.Services.Entities;
using RecruitmentPortal.Services.Interfaces;
using RecruitmentPortal.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecruitmentPortal.Controllers
{

    public class ApplicationController : Controller
    {
        IApplication _iApplication;
        IMail _iMail;
        public ApplicationController(IApplication iApplication, IMail iMail)
        {
            _iApplication = iApplication;
            _iMail = iMail;
        }
        [Route("{id}")]
        public ActionResult Details(int id)
        {
            //...code to extract pdf from SQLServer and store in stream
            var file = _iApplication.GetPDF(id);
            return View(file);
        }

        [Authorize (Roles = "APPLICANT")]
        // GET: Application
        public ActionResult Index(int id)
        {
            ApplicationViewModel model = new ApplicationViewModel();
            model.Jobs = _iApplication.GetJobs(id);
            return View(model);
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Applications application, HttpPostedFileBase file, int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Method 1: Get file details from current request
                    if (Request.Files.Count > 0)
                    {
                        var uploadfile = Request.Files[0];
                        if (uploadfile != null && uploadfile.ContentLength > 0)
                        {
                            _iApplication.InsertApplications(application, uploadfile, id);
                            var user = _iMail.GetCurrentUser();
                            var heading = _iMail.Heading();
                            var message = _iMail.Message();
                            _iMail.SendEmailAsync(user, heading, message);
                            return RedirectToAction("Enquiry", "Application");
                        }
                    }
                }
                catch (Exception)
                {
                    ViewBag.Status = "Error while uploading file";
                }
            }
            return View();
        }
        //Admin can view list of applicants submission
        [Authorize (Roles ="Admin")]
        public ActionResult Applicants()
        {
            var application = _iApplication.GetApplication();
            return View(application);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Applicant()
        {
            var application = _iApplication.GetApplication();
            return View(application);
        }
        [HttpGet]
        public ActionResult Status(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var application = _iApplication.GetApplicationById(id.Value);
                return View(application);
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        public ActionResult Enquiry()
        {
            return View();
        }
        public ActionResult AllApplications()
        
        {
            var applications = _iApplication.SpecificApplication();
            return View(applications);
        }
        [HttpGet]
        public ActionResult Accept(int id)
        {
            var result = _iApplication.AcceptApplications(id);
            var heading = _iMail.Heading2();
            var message = _iMail.Message2();
            _iMail.SendEmailAsync(result, heading, message);
            return RedirectToAction("Applicants", "Application");
        }
        [HttpGet]
        public ActionResult Reject(int id)
        {
            _iApplication.RejectApplication(id);
            var heading = _iMail.Heading3();
            var message = _iMail.Message3();
            _iMail.SendEmailAsync("idamcir@gmail.com", heading, message);
            return RedirectToAction("Applicants", "Application");
        }
    }
}