﻿using RecruitmentPortal.Services.Entities;
using RecruitmentPortal.Services.Interfaces;
using RecruitmentPortal.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace RecruitmentPortal.Controllers
{
    public class JobsController : Controller
    {
        IJobs _iJobs; ICategory _iCategory;
        public JobsController(IJobs iJobs, ICategory iCategory)
        {
            _iJobs = iJobs;
            _iCategory = iCategory;
        }
        // GET: Jobs
        public ActionResult Index()
        {
            var jobs = _iJobs.GetJobs();
            return View(jobs);
        }
        [Authorize (Roles ="APPLICANT")]
        public ActionResult AllJobs()
        {
            var jobs = _iJobs.GetJobs();
            return View(jobs);
        }
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(_iJobs.Any(), "Id", "CategoryName");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Jobs jobs)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.CategoryID = new SelectList(_iJobs.Any(), "Id", "CategoryName", jobs.CategoryID);
                return View(jobs);
            }
                _iJobs.InsertJobs(jobs);
                return RedirectToAction("Index");
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var product = _iJobs.GetJobsById(id.Value);
                return View(product);
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var product = _iJobs.GetJobsById(id.Value);
                return View(product);
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _iJobs.DeleteJobsById(id);
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Jobs jobs = _iJobs.GetJobsById(id.Value);

            if (jobs == null)
                return HttpNotFound();

            ViewBag.CategoryID = new SelectList(_iJobs.Any(), "Id", "CategoryName", jobs.CategoryID);
            return View(jobs);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Jobs jobs)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.CategoryID = new SelectList(_iJobs.Any(), "Id", "CategoryName", jobs.CategoryID);
                return View(jobs);
            }
            _iJobs.UpdateJobs(jobs);
            return RedirectToAction("Index");
        }
        /*public ActionResult SearchResult(string keyword, string location, string category)
        {
            var model = new IndexViewModel
            {
                Jobs = _iJobs.SearchResult(keyword, location, category),
                Categories = _iCategory.GetCategories()
            };

            return View(model);
        }*/
    }
}