﻿using RecruitmentPortal.Models;
using RecruitmentPortal.Services.Entities;
using RecruitmentPortal.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecruitmentPortal.Controllers
{
    public class DocumentsController : Controller
    {
        ApplicationDbContext _db;
        IDocuments _iDocuments;
        public DocumentsController(IDocuments iDocuments, ApplicationDbContext db)
        {
            _iDocuments = iDocuments;
            _db = db;
        }
        // GET: Documents
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult CreateDocument()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDocument(Document document, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Method 1: Get file details from current request
                    if(Request.Files.Count > 0)
                    {
                        var uploadfile = Request.Files[0];
                        if(uploadfile != null && uploadfile.ContentLength > 0)
                        {
                            _iDocuments.InsertDoc(document, uploadfile);
                        }
                    }
                    ViewBag.Status = "Uploaded Successfully";
                }
                catch (Exception)
                {
                    ViewBag.Status = "Error while uploading file";
                }           
            }
            return View();
        }
    }
}