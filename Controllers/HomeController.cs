﻿using RecruitmentPortal.Services.Interfaces;
using RecruitmentPortal.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecruitmentPortal.Controllers
{
    public class HomeController : Controller
    {
        IJobs _iJobs; ICategory _iCategory;
        public HomeController(IJobs iJobs, ICategory iCategory)
        {
            _iJobs = iJobs;
            _iCategory = iCategory;
        }
        public ActionResult Index()
        {
            var model = new IndexViewModel
            {
                Jobs = _iJobs.LimitedJobs(),
                Categories = _iCategory.GetCategories()
            };
            /*var jobs = _iJobs.LimitedJobs();*/
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
       
    }
}