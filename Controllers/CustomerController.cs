﻿using Microsoft.AspNet.Identity.Owin;
using RecruitmentPortal.Models;
using RecruitmentPortal.Services.Entities;
using RecruitmentPortal.Services.Interfaces;
using RecruitmentPortal.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace RecruitmentPortal.Controllers
{
    public class CustomerController : Controller
    {
        private ICustomer _iCustomer;
        private ApplicationUserManager _applicationUserManager;
        private ApplicationSignInManager _applicationSignInManager;
        // GET: Customer
        public CustomerController(ICustomer iCustomer, ApplicationUserManager applicationUserManager, ApplicationSignInManager applicationSignInManager)
        {
            _iCustomer = iCustomer;
            _applicationUserManager = applicationUserManager;
            _applicationSignInManager = applicationSignInManager;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ApplicantRegistration()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ApplicantRegistration(Customer customer, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Method 1: Get file details from current request
                    if (Request.Files.Count > 0)
                    {
                        var uploadfile = Request.Files[0];
                        if (uploadfile != null && uploadfile.ContentLength > 0)
                        {
                           var result =  _iCustomer.ApplicaantRegister(customer, uploadfile);
                            return RedirectToAction("ApplicantLogin", "Customer");
                        }
                        else
                        {

                        }
                    }
                    else
                    {

                    }
                }
                catch (Exception)
                {
                    ViewBag.Status = "Error while uploading file";
                }
            }
            return View();
        }
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult EmployerRegistration()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ApplicantLogin(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ApplicantLogin(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var boolResult = _iCustomer.ApplicantLogin(model);
            if (!boolResult)
            {
                return RedirectToAction("ApplicantLogin", "Customer");
            }
            var result = await _applicationSignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
        [Authorize (Roles ="APPLICANT")]
        public ActionResult Dashboard()
        {
            var viewModel = new ApplicationViewModel
            {
                CustomerId = _iCustomer.CustomerId()
            };
            return View(viewModel);
        }
        [Authorize(Roles = "APPLICANT")]
        public ActionResult Update(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var category = _iCustomer.GetCustomerById(id.Value);
                return View(category);
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Update(Customer customer, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Method 1: Get file details from current request
                    if (Request.Files.Count > 0)
                    {
                        var uploadfile = Request.Files[0];
                        if (uploadfile != null && uploadfile.ContentLength > 0)
                        {
                            _iCustomer.UpdateProfile(customer, uploadfile);
                            return RedirectToAction("Dashboard", "Customer");
                        }
                        else
                        {

                        }
                    }
                    else
                    {

                    }
                }
                catch (Exception)
                {
                    ViewBag.Status = "Error while uploading file";
                }
            }
            return View();
        }
    }
}