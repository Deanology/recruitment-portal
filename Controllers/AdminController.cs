﻿using RecruitmentPortal.Services.Interfaces;
using RecruitmentPortal.Services.Repositories;
using RecruitmentPortal.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecruitmentPortal.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        IAdmin _iAdmin;
        public AdminController(IAdmin iAdmin)
        {
            _iAdmin = iAdmin;
        }
        // GET: Admin

        public ActionResult Index()
        {
            var model = new ApplicantViewModel
            {
                NumberOfJobs = _iAdmin.NumberOfJobs(),
                PendingRequests = _iAdmin.PendingRequests(),
                AcceptedRequests = _iAdmin.AcceptedRequests(),
                RejectedRequests = _iAdmin.RejectedRequests(),
                Categories = _iAdmin.Categories(),
                Applicants = _iAdmin.Applicants(),
                Users = _iAdmin.Users()
            };
            return View(model);
        }
        public ActionResult JobsApplicants() { 

            var jobs = _iAdmin.GetJobs();
            return View(jobs);
        }
        public ActionResult ApplicantJobs(int id)
        {
            var applications = _iAdmin.GetApplications(id);
            return View(applications);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult AllUsers()
        {
            var users = _iAdmin.GetCustomers();
            return View(users);
        }
    }
}