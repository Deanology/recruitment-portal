﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "file", c => c.String(nullable: false));
            DropColumn("dbo.Customers", "Image");
            DropColumn("dbo.Customers", "CV");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "CV", c => c.String());
            AddColumn("dbo.Customers", "Image", c => c.String());
            DropColumn("dbo.Customers", "file");
        }
    }
}
