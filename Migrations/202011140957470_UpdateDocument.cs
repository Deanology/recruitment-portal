﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDocument : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Documents", "Pdf");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Documents", "Pdf", c => c.String());
        }
    }
}
