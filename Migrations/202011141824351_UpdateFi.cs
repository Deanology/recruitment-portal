﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFi : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Documents", "file", c => c.String());
            DropColumn("dbo.Documents", "Image");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Documents", "Image", c => c.String());
            DropColumn("dbo.Documents", "file");
        }
    }
}
