﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateEntity1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Applications", "ApplicationStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Applications", "ApplicationStatus", c => c.String());
        }
    }
}
