﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateJob : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Jobs", "JobPosition", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Jobs", "JobPosition");
        }
    }
}
