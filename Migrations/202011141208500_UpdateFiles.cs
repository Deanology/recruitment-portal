﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFiles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Documents", "Image", c => c.String());
            DropColumn("dbo.Documents", "Uploadfile");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Documents", "Uploadfile", c => c.String());
            DropColumn("dbo.Documents", "Image");
        }
    }
}
