﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PreviousEmail : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Emails");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        To = c.String(),
                        From = c.String(),
                        Subject = c.String(),
                        Body = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
