﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateJobs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Jobs", "file", c => c.String(nullable: false));
            DropColumn("dbo.Jobs", "JobLogo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Jobs", "JobLogo", c => c.String());
            DropColumn("dbo.Jobs", "file");
        }
    }
}
