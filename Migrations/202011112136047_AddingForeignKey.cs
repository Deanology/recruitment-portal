﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingForeignKey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Applications", "CustomerId", c => c.Int(nullable: false));
            AlterColumn("dbo.Jobs", "JobTitle", c => c.String(nullable: false, maxLength: 160));
            AlterColumn("dbo.Jobs", "JobType", c => c.String(nullable: false, maxLength: 160));
            AlterColumn("dbo.Jobs", "JobPosition", c => c.String(nullable: false, maxLength: 160));
            AlterColumn("dbo.Jobs", "EmployerName", c => c.String(nullable: false, maxLength: 160));
            AlterColumn("dbo.Jobs", "EmployerLocation", c => c.String(nullable: false, maxLength: 160));
            CreateIndex("dbo.Applications", "CustomerId");
            AddForeignKey("dbo.Applications", "CustomerId", "dbo.Customers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Applications", "CustomerId", "dbo.Customers");
            DropIndex("dbo.Applications", new[] { "CustomerId" });
            AlterColumn("dbo.Jobs", "EmployerLocation", c => c.String());
            AlterColumn("dbo.Jobs", "EmployerName", c => c.String());
            AlterColumn("dbo.Jobs", "JobPosition", c => c.String());
            AlterColumn("dbo.Jobs", "JobType", c => c.String());
            AlterColumn("dbo.Jobs", "JobTitle", c => c.String());
            DropColumn("dbo.Applications", "CustomerId");
        }
    }
}
