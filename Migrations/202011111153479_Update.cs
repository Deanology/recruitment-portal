﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Customers", "Uimg");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "Uimg", c => c.String(nullable: false));
        }
    }
}
