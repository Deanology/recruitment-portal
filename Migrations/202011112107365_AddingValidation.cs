﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingValidation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "Address", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Customers", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Customers", "FirstName", c => c.String(nullable: false, maxLength: 160));
            AlterColumn("dbo.Customers", "Lastnamme", c => c.String(nullable: false, maxLength: 160));
            AlterColumn("dbo.Customers", "Gender", c => c.String(nullable: false, maxLength: 24));
            AlterColumn("dbo.Customers", "State", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Customers", "PhoneNumber", c => c.String(nullable: false, maxLength: 24));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customers", "PhoneNumber", c => c.String());
            AlterColumn("dbo.Customers", "State", c => c.String());
            AlterColumn("dbo.Customers", "Gender", c => c.String());
            AlterColumn("dbo.Customers", "Lastnamme", c => c.String());
            AlterColumn("dbo.Customers", "FirstName", c => c.String());
            AlterColumn("dbo.Customers", "Email", c => c.String());
            DropColumn("dbo.Customers", "Address");
        }
    }
}
