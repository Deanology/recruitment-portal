﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Applications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateOfApplication = c.DateTime(precision: 7, storeType: "datetime2"),
                        ApplicationStatus = c.String(),
                        JobsId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Jobs", t => t.JobsId, cascadeDelete: true)
                .Index(t => t.JobsId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Applications", "JobsId", "dbo.Jobs");
            DropIndex("dbo.Applications", new[] { "JobsId" });
            DropTable("dbo.Applications");
        }
    }
}
