﻿// <auto-generated />
namespace RecruitmentPortal.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class CustomerEntities : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CustomerEntities));
        
        string IMigrationMetadata.Id
        {
            get { return "202011142227006_CustomerEntities"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
