﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class JobsEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Jobs", "JobTitle", c => c.String());
            AddColumn("dbo.Jobs", "JobType", c => c.String());
            AddColumn("dbo.Jobs", "JobLogo", c => c.String());
            AddColumn("dbo.Jobs", "EmployerName", c => c.String());
            AddColumn("dbo.Jobs", "EmployerLocation", c => c.String());
            AddColumn("dbo.Jobs", "JobDescription", c => c.String());
            AddColumn("dbo.Jobs", "UploadDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
            DropColumn("dbo.Jobs", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Jobs", "Description", c => c.String());
            DropColumn("dbo.Jobs", "UploadDate");
            DropColumn("dbo.Jobs", "JobDescription");
            DropColumn("dbo.Jobs", "EmployerLocation");
            DropColumn("dbo.Jobs", "EmployerName");
            DropColumn("dbo.Jobs", "JobLogo");
            DropColumn("dbo.Jobs", "JobType");
            DropColumn("dbo.Jobs", "JobTitle");
        }
    }
}
