﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApplicationEntities : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Applications", "file", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Applications", "file");
        }
    }
}
