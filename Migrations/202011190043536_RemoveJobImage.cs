﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveJobImage : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Jobs", "file");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Jobs", "file", c => c.String(nullable: false));
        }
    }
}
