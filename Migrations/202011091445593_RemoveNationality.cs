﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveNationality : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "State", c => c.String());
            DropColumn("dbo.Customers", "Location");
            DropColumn("dbo.Customers", "Nationality");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "Nationality", c => c.String());
            AddColumn("dbo.Customers", "Location", c => c.String());
            DropColumn("dbo.Customers", "State");
        }
    }
}
