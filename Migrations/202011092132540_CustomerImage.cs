﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerImage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "Uimg", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "Uimg");
        }
    }
}
