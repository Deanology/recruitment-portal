﻿namespace RecruitmentPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class JobsEntity2 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Jobs", name: "Category_Id", newName: "CategoryID");
            RenameIndex(table: "dbo.Jobs", name: "IX_Category_Id", newName: "IX_CategoryID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Jobs", name: "IX_CategoryID", newName: "IX_Category_Id");
            RenameColumn(table: "dbo.Jobs", name: "CategoryID", newName: "Category_Id");
        }
    }
}
